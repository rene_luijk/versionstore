use git2::{IndexAddOption, Repository};
use std::fs::File;
use std::io::prelude::*;
use url::Url;

/// The web files that are retrieved by the
/// scraping part of the application should each
/// use this WebFile structure as a first step to be committed.
#[derive(Debug)]
pub struct WebFile {
    pub name: String,
    pub content: String,
    /// The Repository Path
    pub destination: String,
}

impl WebFile {
    /// To prepare the saving to the file storage that can be picked up
    /// later by the add_all and commit functions, use the new method
    /// and save directly afterwards
    pub fn new(name: Url, content: String, destination: String) -> Self {
        let name = WebFile::webfile_name(name);
        WebFile {
            name,
            content,
            destination,
        }
    }

    /// Can be called directly after the new method if needed.
    pub fn save(&self) -> std::io::Result<()> {
        let path = format!("{}/{}", &self.destination, self.name);
        let mut file = File::create(&path)?;
        file.write_all(self.content.as_bytes())?;
        Ok(())
    }
    /// Retrieve a filename for storage of the content
    /// test
    pub fn webfile_name(url: Url) -> String {
        let file_host = url.host().unwrap().to_string();
        let path = url.path().to_string().replace(".html", "");
        let file_name = format!("{}.{}.html", file_host, path).replace("/", "");
        file_name
    }
}

/// When the scraping part is done, you can call add_all to prepare the
/// index to be used by the commit function.
pub fn add_all(destination: String) {
    let repo = Repository::open(destination).expect("failed to open");
    let mut index = repo.index().expect("cannot get the Index file");
    let _ = index.add_all(["*"].iter(), IndexAddOption::DEFAULT, None);
    let _ = index.write();
}

/// Opens the repo, turn the index into a tree, and add the tree as a commit to HEAD
/// Make sure to call the add_all method first.
pub fn commit(message: &str, destination: String) {
    // commit and making that be the parent of the initial commit
    let repo = Repository::open(destination).expect("failed to open");
    let sig = repo.signature().unwrap();
    let head = match repo.head() {
        Ok(head) => head,
        Err(e) => panic!("{}", e),
    };

    // by peeling it.
    let parent_commit = match head.peel_to_commit() {
        Ok(commit) => commit,
        Err(e) => panic!("{}", e),
    };

    //let tree = parent_commit.tree().unwrap();
    let mut index = repo.index().unwrap();
    let tree_id = index.write_tree().unwrap(); // oid to be used to create commit
    let tree = repo.find_tree(tree_id).unwrap();

    //let message = message;
    let _ = repo.commit(Some("HEAD"), &sig, &sig, message, &tree, &[&parent_commit]);
}

// Simple version that assumes destination is same repo for all web files
pub fn commit_files(web_files: &mut Vec<WebFile>, message: &str) {
    let destination = web_files[0].destination.clone();
    for web_file in web_files {
        let _ = web_file.save();
    }
    //stage & commit
    add_all(destination.clone());
    commit(message, destination.clone());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_commit_files() {
        let mut web_files = Vec::new();
        let web_file1 = WebFile::new(
            "webfile1.html".to_string(),
            "<HTML>test webfile 1</HTML>".to_string(),
            "D:/repos/repo123".to_string(),
        );
        let web_file2 = WebFile::new(
            "webfile2.html".to_string(),
            "<HTML>test webfile 2</HTML>".to_string(),
            "D:/repos/repo123".to_string(),
        );
        web_files.push(web_file1);
        web_files.push(web_file2);
        commit_files(&mut web_files, "test 2 commit files");
    }

    #[test]
    fn test_storage() -> std::io::Result<()> {
        let destination = "D:/repos/repo123/".to_string();
        let web_file = WebFile::new(
            "test41.html".to_string(),
            "<HTML>test41</HTML>".to_string(),
            destination,
        );
        web_file.save();

        let mut file = File::open("D:/repos/repo123/test41.html")?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;
        assert_eq!(contents, "<HTML>test41</HTML>");
        Ok(())
    }

    #[test]
    fn test_add_all() {
        let destination = "D:/repos/repo123".to_string();
        add_all(destination);
    }
    #[test]
    fn test_commit() {
        let destination = "D:/repos/repo123".to_string();
        commit("some test auto again destiny 3...", destination);
    }
}
