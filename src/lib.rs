//! This crate takes care of the version store
//! It exposes the essential API to access the underlying commits
//#[macro_use]
extern crate chrono;
extern crate git2;

pub mod webfilestore;
pub use webfilestore::{add_all, commit, commit_files, WebFile};
